const express = require("express");
const app = express();

app.use(express.static(__dirname + "/static"));

const posts = [
  {
    id: 1,
    title: "First post",
    content: "Content of my first post",
  },
  {
    id: 2,
    title: "Second post",
    content: "Content of my first post",
  },
  {
    id: 3,
    title: "Third post",
    content: "Content of my first post",
  },
];

app.get("/api/title", (req, res) => {
  res.send("Titolo hello world");
});

app.get("/api/posts", (req, res) => {
  res.send(posts);
});

app.listen(3000, () => {
  console.log("Listening on 3000");
});
