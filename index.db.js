const express = require("express");
let createDatabase = require("./db-connection").createDatabase;

let Database;

const app = express();

app.use(express.json());

app.get("/", async (req, res) => {
  const posts = await Database.Post.findAll();
  return res.send(posts);
});

app.post("/", async (req, res) => {
  const body = req.body;
  const newPost = await Database.Post.create({
    title: body.title,
    content: body.content,
    image: body.image,
  });
  return res.send(newPost);
});

app.listen(3000, async () => {
  Database = await createDatabase();
  console.log("Listening on port 3000");
});
