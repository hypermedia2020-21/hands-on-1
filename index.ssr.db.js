const express = require("express");
const createDatabase = require("./db-connection").createDatabase;
const app = express();
app.use(express.json());
let Database;

function composePost({ id, title, content, image }) {
  return `
    <div id="post-${id}" class="post">
        <h3>${title}</h3>
        <img src="${image}">
        <p>${content}</p>
    </div>
    `;
}

app.get("/", async (req, res) => {
  let text = "<h1>Titolo hello world</h1>";
  const posts = await Database.Post.findAll();
  text +=
    `<div class="post-container">` +
    posts
      .map((singlePost) => {
        return composePost(singlePost);
      })
      .join("") +
    `</div>`;
  let final = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
    ${text}
    </body>
</html>`;
  res.send(final);
}); 


app.post("/", async (req, res) => {
  const body = req.body;
  const created = await Database.Post.create(body);
  return res.send(created);
});


app.put("/:id", async (req, res) => {
  const body = req.body;
  const updated = await Database.Post.update(body, {
    where: { id: req.params.id },
  });
  return res.send(updated);
});

app.use(express.static(__dirname + "/static"));

app.listen(3000, async () => {
  Database = await createDatabase();
  console.log("Listening on 3000");
});
