function composePost({ id, title, content }) {
  return `
  <div id="post-${id}" class="post">
      <h3>${title}</h3>
      <p>${content}</p>
  </div>
  `;
}
initContent();

function initContent() {
  // Get title
  getTitle();
  // Get post
  getPost();
}

async function getTitle() {
  const response = await fetch("http://localhost:3000/api/title");
  const data = await response.text();
  const myTitleHTMLElement = document.getElementById("my_title");
  myTitleHTMLElement.innerText = data;
}

async function getPost() {
  const response = await fetch("http://localhost:3000/api/posts");
  const data = await response.json();
  let final = ``;
  for (const post of data) {
    final += composePost(post);
  }
  const postContainerHTMLElement = document.querySelector(".post-container");
  postContainerHTMLElement.innerHTML = final;
}
