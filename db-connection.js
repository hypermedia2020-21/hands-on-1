const { Sequelize, DataTypes } = require("sequelize");

const db = new Sequelize(
  "postgres://postgres:test123@localhost:5432/hypermedia-test"
);

function defineDatabase() {
  db.Post = db.define("posts", {
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    image: DataTypes.STRING,
  });
  db.Comment = db.define("comments", {
    content: DataTypes.TEXT,
  });
  db.Post.hasMany(db.Comment);
}

async function createDatabase() {
  try {
    defineDatabase();
    await db.sync();
    console.log("connected");
    return db;
  } catch (err) {
    console.log("error", err);
  }
}

module.exports = {
    createDatabase
};
