const express = require("express");

const app = express();

const posts = [
  {
    id: 1,
    title: "First post",
    content: "Content of my first post",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png",
  },
  {
    id: 2,
    title: "Second post",
    content: "Content of my first post",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png",
  },
  {
    id: 3,
    title: "Third post",
    content: "Content of my first post",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png",
  },
];

function composePost({ id, title, content, image }) {
  return `
    <div id="post-${id}" class="post">
        <h3>${title}</h3>
        <img src="${image}">
        <p>${content}</p>
    </div>
    `;
}

app.get("/", (req, res) => {
  let text = "<h1>Titolo hello world</h1>";
  text +=
    `<div class="post-container">` +
    posts
      .map((singlePost) => {
        return composePost(singlePost);
      })
      .join("") +
    `</div>`;
  let final = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
    ${text}
    </body>
</html>`;

  res.send(final);
});

app.use(express.static(__dirname + "/static"));

app.listen(3000, () => {
  console.log("Listening on 3000");
});
